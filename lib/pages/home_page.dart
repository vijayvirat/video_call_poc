import 'package:alphabet_list_scroll_view/alphabet_list_scroll_view.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:videocall_poc/pages/call_page.dart';
import 'package:videocall_poc/utils/colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List userList = [
    {"id": 1, "name": "Iron man"},
    {"id": 2, "name": "Captain america"},
    {"id": 3, "name": "Hulk"},
    {"id": 4, "name": "Black Widow"},
    {"id": 5, "name": "Bat Man"},
    {"id": 6, "name": "Spider Man"},
    {"id": 7, "name": "Thanos"},
    {"id": 8, "name": "Thor"},
    {"id": 9, "name": "Rocket"},
    {"id": 10, "name": "Groot"},
    {"id": 11, "name": "X-man"},
    {"id": 12, "name": "Doctor strange"},
    {"id": 13, "name": "Bruce"},
    {"id": 14, "name": "Ant man"},
    {"id": 15, "name": "Eternals"},
    {"id": 16, "name": "Fantastic four"},
    {"id": 17, "name": "Joker"},
    {"id": 18, "name": "Kong"},
    {"id": 19, "name": "Lion man"},
    {"id": 20, "name": "Mark"},
    {"id": 21, "name": "Natasha"},
    {"id": 22, "name": "Octobus"},
    {"id": 23, "name": "Panther"},
    {"id": 24, "name": "Quntum"},
    {"id": 25, "name": "Ultra man"},
    {"id": 26, "name": "Vilian"}
  ];
  List<String> strList = [];
  TextEditingController searchController = TextEditingController();
  late int selectedId = 0;

  @override
  void initState() {
    userList.sort(
        (a, b) => a["name"].toLowerCase().compareTo(b["name"].toLowerCase()));
    filterList();
    searchController.addListener(() {
      filterList();
    });
    super.initState();
  }

  filterList() {
    if (searchController.text.isNotEmpty) {
      userList.retainWhere((user) => user["name"]
          .toLowerCase()
          .contains(searchController.text.toLowerCase()));
    }

    userList.forEach((e) {
      strList.add(e["name"]);
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          Opacity(
            opacity: 0.7,
            child: Image.asset(
              "assets/building.jpeg",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: PreferredSize(
                preferredSize: const Size.fromHeight(45.0),
                child: AppBar(
                  elevation: 0.7,
                  backgroundColor: hexToColor(lightGray),
                  title: const Text("Sprngpod"),
                  actions: [
                    Container(
                      color: hexToColor(darkGray),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Text(
                                  "Guest Wifi",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "C1tyL1nk3@!",
                                  style: TextStyle(fontSize: 10),
                                )
                              ],
                            ),
                            const SizedBox(
                              width: 14.0,
                            ),
                            const Icon(
                              Icons.wifi,
                              size: 40,
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
            body: Container(
              padding:
                  const EdgeInsets.only(top: 10.0, left: 15.0, right: 15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: [
                      InkWell(
                        onTap: () {
                          // Navigator.pop(context);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(7.0),
                          decoration: BoxDecoration(
                              color: hexToColor(darkGray),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: const Icon(
                            Icons.arrow_back_ios_new,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const Text(
                        "Back",
                        style: TextStyle(color: Colors.white),
                      )
                    ],
                  ),
                  Expanded(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          decoration: BoxDecoration(
                              color: hexToColor(darkGray),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(20.0))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                child: Column(
                                  children: [
                                    Row(
                                      children: const [
                                        Icon(
                                          Icons.contact_phone,
                                          color: Colors.white,
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        Text("Directory",
                                            style:
                                                TextStyle(color: Colors.white))
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0.0, vertical: 15.0),
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0, horizontal: 10.0),
                                        decoration: BoxDecoration(
                                            color: hexToColor(searchBar),
                                            borderRadius:
                                                BorderRadius.circular(30.0)),
                                        child: Column(
                                          children: const [
                                            TextField(
                                              // controller: searchController,
                                              decoration:
                                                  InputDecoration.collapsed(
                                                      hintText:
                                                          'Search Resident'),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(child: _buildList())
                            ],
                          ),
                        )),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              height: 60.0,
              color: hexToColor(darkGray),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    "assets/uber.png",
                    height: 35,
                  ),
                  const SizedBox(
                    width: 15.0,
                  ),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Image.asset(
                      "assets/person.jpeg",
                      height: 50,
                    ),
                  ),
                  const SizedBox(
                    width: 15.0,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> onJoin() async {
    await _handleCameraAndMic(Permission.camera);
    await _handleCameraAndMic(Permission.microphone);

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const CallPage(channelName: "test"),
        ));
  }

  Future<void> _handleCameraAndMic(Permission permission) async {
    final status = await permission.request();
    print(status);
  }

  _buildList() {
    return AlphabetListScrollView(
      strList: strList,
      highlightTextStyle: const TextStyle(
        color: Colors.white,
      ),
      normalTextStyle: const TextStyle(color: Colors.white),
      showPreview: true,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            setState(() {
              selectedId = userList[index]["id"];
            });
          },
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 5.0),
            padding: EdgeInsets.only(
                right: MediaQuery.of(context).size.width / 7, left: 5.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      userList[index]["name"],
                      style:
                          const TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                    userList[index]["id"] == selectedId
                        ? SizedBox(
                            height: 28,
                            child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            hexToColor(highlight))),
                                onPressed: onJoin,
                                child: const Text("CALL")))
                        : const SizedBox()
                  ],
                ),
                const Divider(
                  color: Colors.white,
                  thickness: 2.0,
                  height: 20,
                )
              ],
            ),
          ),
        );
      },
      indexedHeight: (i) {
        return 60;
      },
      keyboardUsage: true,
      headerWidgetList: <AlphabetScrollListHeader>[
        AlphabetScrollListHeader(widgetList: [
          // Padding(
          //   padding:
          //       const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
          //   child: Container(
          //     padding:
          //         const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          //     decoration: BoxDecoration(
          //         color: hexToColor(searchBar),
          //         borderRadius: BorderRadius.circular(30.0)),
          //     child: Column(
          //       children: const [
          //         TextField(
          //           // controller: searchController,
          //           decoration:
          //               InputDecoration.collapsed(hintText: 'Search Resident'),
          //         ),
          //       ],
          //     ),
          //   ),
          // )
        ], icon: const Icon(Icons.search), indexedHeaderHeight: (index) => 70)
      ],
    );
  }
}
