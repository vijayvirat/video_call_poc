import 'dart:ui';

import 'package:flutter/material.dart';

hexToColor(String? code) {
  return Color(int.parse(code!.substring(1, 7), radix: 16) + 0xFF000000);
}

const primaryBlue = "#044DA1";
const greenColor = "#0CDFA3";
const whiteColor = "#ffffff";
const lightBlueColor = "#DCF7FF";
const darkGray = "#38465F";
const lightGray = "#a7abb0";
const highlight = "#90ABCE";
const searchBar = "#EEF5FF";
